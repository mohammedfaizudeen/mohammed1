class A:
    def __init__(self, a):
        self.a = a

class B(A):
    def __init__(self):
        self.b_a = self.a
        print('hh')

a = A('a')
b = B()
print(b.a)

