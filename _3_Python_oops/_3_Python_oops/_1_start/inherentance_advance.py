class Person:
    def __init__(self, obj):
        self.obj = obj

    def whoIsThis(self, who):
        print('he is {}'.format(who))

class Father(Person):
    def __init__(self, name):
        self.name = name

class Boy(Person):

    def __init__(self, name):
        self.name = name

class Girl(Person):

    def __init__(self, name):
        self.name = name

f = Father('John')
b = Boy('Mavericjk')
g = Girl('leena')

print('father is {}'.format(f.name)+'boy is {}'.format(b.name)+'girs is {}'.format(g.name))
f.whoIsThis('john')
b.whoIsThis('maverick')
g.whoIsThis('leena')
