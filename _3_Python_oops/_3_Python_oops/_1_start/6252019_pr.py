class Employee:

    def __init__(self, id, name, address):
        print('i will be called when'
              'you will create object of a this class')
        self.emp_id = id
        self.emp_name = name
        self.emp_address = address

    def print_details(self):
        print(self.emp_id)


class Student:

    def __init__(self, id, name, address):
        print('i will be called when'
              'you will create object of a this class')
        self.emp_id = id
        self.emp_name = name
        self.emp_address = address

    def print_details(self):
        print(self.emp_id)


obj = Employee('111', 'john', 'ny')
print(obj.emp_id)
print(obj.emp_name)
print(obj.emp_address)

obj.print_details()

objst = Student('122', 'mark', 'ny')
objst.print_details()

