#!/usr/bin/env python

class Human:
    def _sayHello(self, name=None):
        if name is not None:
            print('Hello ' + name)
        else:
            print('Hello ')




# Create instance
obj = Human()
# Call the method
obj._sayHello()


# Call the method with a parameter
obj._sayHello('john')
_sayHello