class Employe(object):

    def __init__(self, id, name, sal):
        self.id = id
        self.name = name
        self.sal = sal

    def print_details(self):
        print(self.id)
        print(self.name)
        print(self.sal)

class Dev(Employe):

    def __init__(self,id, name, sal):
        Employe.__init__(self,id, name, sal)

class Sale(Employe):

    def __init__(self,id, name, sal, allw, tar):
        Employe.__init__(self, id, name, sal)
        self.allw = allw
        self.tar = tar

d = Dev(111, 'john', 20000)
d.print_details()

