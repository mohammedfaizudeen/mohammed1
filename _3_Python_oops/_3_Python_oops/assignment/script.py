import os

def main_file():
    # define the name of the directory to be created
    path = "Root/python1"

    try:
        os.makedirs(path)
    except OSError:
        print ("Creation of the directory %s failed" % path)
    else:
        print ("Successfully created the directory %s" % path)

    f = open('Root/python1/python1.py', 'w+')
    f.write("hello python 1")

print(main_file())

# i = main_file()
# for i in range(10):
#     main_file()
#     i += 1


# f.seek(0)
# data = f.read()
# print(data)

# f = open('G/root/python1/python1.py', 'w+')
# for i in range(10) :
#     f.write("hello python %d\r\n" % (i+1))
# f.close()

