class SimpleClass:

    def __init__(self):
        self.__datacamp = "Excellent"

    def get_datacamp(self):
        return self.__datacamp


obj = SimpleClass()
print(obj.get_datacamp())  ## it prints the "Excellent" which is a __var
#print(obj.__datacamp)
